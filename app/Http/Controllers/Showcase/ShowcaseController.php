<?php

namespace App\Http\Controllers\Showcase;

use App\Http\Controllers\Controller;

class ShowcaseController extends Controller
{
    public function home()
    {
        return view('showcase.home');
    }
}
