<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        $request->validate([
            'firstname' => 'nullable|string|max:255',
            'lastname' => 'nullable|string|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        Auth::login($user);

        return redirect()->route('dashboard.home');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function postLogin(Guard $auth, Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$auth->attempt($credentials)) {
            return redirect()->back()->withErrors(['Adresse e-mail ou mot de passe incorrect.']);
        }

        return redirect()->route('dashboard.home');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('showcase.home');
    }
}
