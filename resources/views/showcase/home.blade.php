@extends('_layouts.showcase')
@section('content')
    <div class="container">
        <ul>
            <li>
                <a href="{{ route('auth.login') }}">Se connecter</a>
            </li>
            <li>
                <a href="{{ route('auth.register') }}">S'inscrire</a>
            </li>
        </ul>
    </div>
@endsection
